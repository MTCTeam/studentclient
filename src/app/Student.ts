export class Student {
  constructor(public id: any,
              public firstName: string,
              public lastName: string,
              public classModel: string,
              public gpa: number,
              public picture: string) {
  }
}
