import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';
import { Student } from '../Student';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  // ***** Member Variables *****
  studentsDatabase: Array<any>;
  studentsGPA: Array<any>;
  studentsPicture: Array<any> = new Array<any>();
  students: Array<Student> = new Array<Student>();
  student: any[];
  showBuyPopup: boolean;

  // ***** Constructors *****
  constructor(public studentService: StudentService) {
  }


  ngOnInit() {
    this.studentService.getAllStudents().subscribe(student => {this.studentsDatabase = student; }, () => {} , () => {
      this.studentService.getGPA().subscribe(gpa => {this.studentsGPA = gpa; }, () => {} , () => {
        this.initializePictures();
        this.initializeStudents();
      });
    });
  }

  initializePictures(): void {
    this.studentsPicture.push('../../assets/images/bananas.png');
    this.studentsPicture.push('../../assets/images/grapes.png');
    this.studentsPicture.push('../../assets/images/kiwi.png');
    this.studentsPicture.push('../../assets/images/pineapple.png');
    this.studentsPicture.push('../../assets/images/redapple.png');
    this.studentsPicture.push('../../assets/images/watermelon.png');
    this.studentsPicture.push('../../assets/images/bananas.png');
    this.studentsPicture.push('../../assets/images/grapes.png');
    this.studentsPicture.push('../../assets/images/kiwi.png');
    this.studentsPicture.push('../../assets/images/pineapple.png');
    this.studentsPicture.push('../../assets/images/redapple.png');
    this.studentsPicture.push('../../assets/images/watermelon.png');
    this.studentsPicture.push('../../assets/images/bananas.png');
    this.studentsPicture.push('../../assets/images/grapes.png');
    this.studentsPicture.push('../../assets/images/kiwi.png');
    this.studentsPicture.push('../../assets/images/pineapple.png');
    this.studentsPicture.push('../../assets/images/redapple.png');
    this.studentsPicture.push('../../assets/images/watermelon.png');
    this.studentsPicture.push('../../assets/images/bananas.png');
    this.studentsPicture.push('../../assets/images/grapes.png');
    this.studentsPicture.push('../../assets/images/kiwi.png');
    this.studentsPicture.push('../../assets/images/pineapple.png');
    this.studentsPicture.push('../../assets/images/redapple.png');
    this.studentsPicture.push('../../assets/images/watermelon.png');
    this.studentsPicture.push('../../assets/images/bananas.png');
    this.studentsPicture.push('../../assets/images/grapes.png');
    this.studentsPicture.push('../../assets/images/kiwi.png');
    this.studentsPicture.push('../../assets/images/pineapple.png');
    this.studentsPicture.push('../../assets/images/redapple.png');
    this.studentsPicture.push('../../assets/images/watermelon.png');
    this.studentsPicture.push('../../assets/images/bananas.png');
    this.studentsPicture.push('../../assets/images/grapes.png');
    this.studentsPicture.push('../../assets/images/kiwi.png');
    this.studentsPicture.push('../../assets/images/pineapple.png');
    this.studentsPicture.push('../../assets/images/redapple.png');
    this.studentsPicture.push('../../assets/images/watermelon.png');
    this.studentsPicture.push('../../assets/images/bananas.png');
    this.studentsPicture.push('../../assets/images/grapes.png');
    this.studentsPicture.push('../../assets/images/kiwi.png');
    this.studentsPicture.push('../../assets/images/pineapple.png');
    this.studentsPicture.push('../../assets/images/redapple.png');
    this.studentsPicture.push('../../assets/images/watermelon.png');
    this.studentsPicture.push('../../assets/images/bananas.png');
    this.studentsPicture.push('../../assets/images/grapes.png');
    this.studentsPicture.push('../../assets/images/kiwi.png');
    this.studentsPicture.push('../../assets/images/pineapple.png');
    this.studentsPicture.push('../../assets/images/redapple.png');
    this.studentsPicture.push('../../assets/images/watermelon.png');
  }

  initializeStudents(): void {
  for (let i = 0; i < this.studentsDatabase.length; i++ ) {
    console.log(i);
    this.students.push(new Student(this.studentsDatabase[i].id,
                                   this.studentsDatabase[i].firstName,
                                   this.studentsDatabase[i].lastName,
                                   this.studentsDatabase[i].classModel.className ? this.studentsDatabase[i].classModel.className : null,
                               this.studentsGPA[this.studentsDatabase[i].id] ? this.studentsGPA[this.studentsDatabase[i].id] : null,
                                   this.studentsPicture[i]));
  }
  for (let i = 0; i < this.studentsGPA.length; i++ ) {
    if (this.studentsGPA[i] != null) {
      this.studentsGPA[i] = this.studentsGPA[i].toFixed(2);
    }
  }
  }

  addGPA(id): void {
    const obj = { grade: this.studentsGPA[id] + 0.5,
                  student: id ? {id} : null};
    this.studentService.addGPA(obj).subscribe();
    console.log('Hello button press', id);
    console.log(this.studentsGPA[id]);
    this.studentsGPA[id] += 0.5;
    console.log('After' + this.studentsGPA[id]);
  }

  testGPA(id, gpa): void {
    console.log('Hello button press', id);
    if (gpa >= 2.0) {
      document.getElementById(id).innerText = 'PASS';
      document.getElementById(id).style.background = 'green';
    } else {
      document.getElementById(id).innerText = 'FAIL';
      document.getElementById(id).style.background = 'red';
    }
  }

}
