import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  // ***** Member Variables *****

  // ***** Constructors *****
  constructor(private http: HttpClient) {
  }

  // ***** Functions *****
  getAllStudents(): Observable<any> {
    return this.http.get('//student-project-terrific-lion.apps.pcf.pcfhost.com/students');
  }

  getGPA(): Observable<any> {
    return this.http.get('//student-project-terrific-lion.apps.pcf.pcfhost.com/average');
  }

  getClasses(): Observable<any> {
    return this.http.get('//student-project-terrific-lion.apps.pcf.pcfhost.com/classes');
  }

  create(item: object) {
    console.log(item);
    return this.http.post('//student-project-terrific-lion.apps.pcf.pcfhost.com/create', item);
  }

  addGPA(item: object) {
    console.log(item);
    return this.http.post('//student-project-terrific-lion.apps.pcf.pcfhost.com/addGPA', item);
  }

}
