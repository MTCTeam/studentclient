import { Component, OnInit } from '@angular/core';
import {StudentService} from '../student.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css']
})
export class CreateStudentComponent implements OnInit {

  // ***** Member Variables *****
  newStudent: FormGroup;
  student: any[];
  classes: Array<any>;

  // ***** Constructors *****
  constructor(public studentService: StudentService) { }

  ngOnInit() {
    this.studentService.getClasses().subscribe( classData => {this.classes = classData; });

    this.newStudent = new FormGroup({
      firstName: new FormControl(),
      lastName: new FormControl(),
      classModel: new FormControl()
    });
  }

  // ***** Functions *****
  createStudent() {
    const obj = {
      firstName: this.newStudent.controls.firstName.value,
      lastName: this.newStudent.controls.lastName.value,
      classModel: this.newStudent.controls.classModel.value ? {id: this.newStudent.controls.classModel.value} : null,
    };
    this.studentService.create(obj).subscribe(data => {this.student = data as any[]; });
    this.newStudent.reset();
  }
}
